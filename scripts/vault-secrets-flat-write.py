#!/usr/bin/env python

description = """

Upload secrets from yaml to vault. Input is a dictionary mapping vault paths to flat secret dictionaries, like so:

/secret/dev/my/db:
    username: admin
    pass: s3cr3tp4ss

/secret/dev/my/apache:
    username: root
    pass: m0r3stVff
    
"""
import argparse

import yaml
import sys, os
import requests 
   
vault_token = None
vault_addr = None
args = None

http_codes = {
    400 : """Invalid request, missing or invalid data. See the \"validation\" section for more details on the error response.""",
    401 : """Unauthorized, your authentication details are either incorrect or you don't have access to this feature.""",    
    403 : """Unauthorized, your authentication details are either incorrect or you don't have access to this feature.""",
    404 : """Invalid path. This can both mean that the path truly doesn't exist or that you don't have permission to view a specific path. We use 404 in some cases to avoid state leakage.""",
    429 : """Rate limit exceeded. Try again after waiting some period of time.""",
    500 : """Internal server error. An internal error has occurred, try again later. If the error persists, report a bug.""",
    503 : """Vault is down for maintenance or is currently sealed. Try again later."""
}

def die(s):
    sys.stderr.write("Error: %s\n" % s)
    sys.exit(1)

def check_init():
    assert(vault_addr!=None), "vault_addr must be initialized"
    assert(vault_token!=None), "vault_token must be initialized"

def get_vault_path(key):
    path = os.path.join(args.prefix, key)
    if path[:1]=='/': # should not start with '/'
      path = path[1:]
    return path

def delete_key(key):
    check_init()
    vault_path = get_vault_path(key)
     # do post request
    url = vault_addr + "v1/" + vault_path
    r = requests.delete(url, 
        headers = {
            "X-Vault-Token": "%s" % vault_token,
            "Content-Type": "application/json"
        }
    )
    ok, err = process_http_response_code(r, vault_path)
    if ok: 
        print("- deleted %s" % (vault_path))
    else:
        sys.stderr.write("! Error writing key '%s': %s\n" % (vault_path, err))


def write_key(key, data):
    check_init()
    
    vault_path = get_vault_path(key)
    
    if data==None:
        print("! Ignoring entry %s (no data)" % (vault_path))
        return
    
    # do post request
    url = vault_addr + "v1/" + vault_path
    
#    url = url.replace('//', '/')
#    url = url.replace(':/', '://')
    try:
        r = requests.post(url, 
            json = data,
            headers = {
                "X-Vault-Token": "%s" % vault_token,
                "Content-Type": "application/json"
            },
            allow_redirects=False # requests lib does not properly post body after redirect, it seems
        )
    except requests.exceptions.ConnectionError as e:
        die("Cannot POST to %s: %s" % (url, str(e)))
    # see https://vaultproject.io/docs/http/
    
    ok, err = process_http_response_code(r, vault_path)
    if ok:
        print("+ wrote secret %s" % (vault_path))
        sys.stdout.flush()
    else:
        sys.stderr.write("! Error writing key '%s': %s\n" % (vault_path, err))

    
    
def process_http_response_code(r, vault_path):
    """ r is response object of Python Requests
    return true iff succesful
    """
    c = r.status_code
    if c==204 or c==200:
        return True, None
        # write ok
    else:
        reason = (c in http_codes and http_codes[c] ) or ("Unknown reason: HTTP code %s; response body: '%s'" % (c, r.text))
        if(args.w):
            return False, reason
        else:
            die("Cannot write vault path %s: %s\n (use -w if you just want a warning and continue)" %(vault_path, reason))
        
def getVaultAddr():
    global vault_addr
    try:
        vault_addr = os.environ['VAULT_ADDR']
    except KeyError as e: 
        die("The VAULT_ADDR environment variable must be set")
    
    # trailing slash is optional
    if vault_addr[-1] != '/':
       vault_addr = vault_addr + '/'
    return vault_addr
    
def getVaultToken():
    global vault_token  
    # open vault token 
    homedir = os.environ['HOME']
    tokenfile = "%s/.vault-token" % homedir
    try: 
        with open(tokenfile, 'r') as stream:
            vault_token = stream.read().strip()
    except IOError as e:
        die("Cannot open tokenfile '%s': %s" % (tokenfile, e))
    return vault_token
 
def main():
    global args
    parser = argparse.ArgumentParser(description=description, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('secretfile', type=str, help='yaml input file containing secrets. use - for stdin')
    parser.add_argument('--prefix', type=str, default='', help='prefix to prepend to secret names (e.g. secret)')
    parser.add_argument('-d', action='store_true', help='delete the entries instead of writing them')
    parser.add_argument('-w', action='store_true', help='warn on errors, do not fail')
    args = parser.parse_args()
    
    vault_token = getVaultToken()
    vault_addr = getVaultAddr()
    
    inputfile = args.secretfile
    
    if inputfile == '-':
        inputfile = '/dev/stdin'
    # open data input file
    try:
        with open(inputfile, 'r') as stream:
            d = yaml.load(stream)
    except yaml.scanner.ScannerError as e:  
        die("Error reading/parsing '%s': %s" % (inputfile, e))
    except IOError as e:
        die("Cannot open secretfile '%s': %s" % (inputfile, e))
        
    if type(d)!=dict:
        die("The file %s does not seem to contain a yaml dictionary." % inputfile)
        
    for key, data in d.iteritems():
        if args.d:
            delete_key(key)
        else:
            write_key(key, data)
        
        
if __name__=="__main__":
    main()
    
