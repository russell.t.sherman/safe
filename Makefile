IMAGE=quay.io/nerdalize/safe

default: help

# build the safe container
build: build-deps-in-container
	docker build -t $(IMAGE) -f Dockerfile .

# build the dependencies for this project; should be run from within container
build-deps:
	go get github.com/feliksik/yaml-go-json

# build the dependencies for this project; should be run from within container
install:
	cp safe /usr/local/bin/safe


####################################
# Stuff below is standard lines to build dependencies in a container
#
# The reason we do this is:
# - we do not want to ship the build toolchain at runtime
# - https://github.com/docker/docker/issues/7115 is not implemented yet
#
# this target can be included in similar projects, too -- provided, they implement a specific build-deps target.
#
# build deps in sub-container
BC_VERSION=0.1
build-deps-in-container:
	mkdir -p deps # assumes by build container
	docker run --rm -ti -v $(shell pwd):/project quay.io/nerdalize/build-container:$(BC_VERSION) bash -c 'cd /project; make build-deps'
	
#shows this help text
help:
	@printf "Available Commands:\n"
	@awk -v sq="'" '/^([a-zA-Z0-9-]*):/ {print "-e " sq NR "p" sq " -e " sq NR-1 "p" sq }' Makefile | while read line; do eval "sed -n $$line Makefile"; done | paste -d"|" - - | sed -e 's/^/  /' -e 's/#//' | awk -F '|' '{ print "  " $$2 "\t" $$1}' | expand -t 24

